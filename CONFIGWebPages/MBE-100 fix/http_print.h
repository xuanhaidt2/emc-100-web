/*****************************************************************************
  AUTO-GENERATED CODE:  Microchip MPFS Generator version: 2.2.2

  Microchip TCP/IP Stack Application Demo

  Company:
    Microchip Technology Inc.

  File Name:
    http_print.h

  Summary:
    This file is automatically generated by the MPFS Generator Utility.
    ALL MODIFICATIONS WILL BE OVERWRITTEN BY THE MPFS GENERATOR.

  Description:
    Provides callback headers and resolution for user's custom
    HTTP Application.
 *****************************************************************************/

// DOM-IGNORE-BEGIN
/*****************************************************************************
Software License Agreement
Copyright(c) 2014 Microchip Technology Inc. All rights reserved.
Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital
signal controller that is integrated into your product or third party
product (pursuant to the sublicense terms in the accompanying license
agreement).

You should refer to the license agreement accompanying this Software
for additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY
OF MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR
PURPOSE. IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR
OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION,
BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT
DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL,
INDIRECT, PUNITIVE OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA,
COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY
CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF),
OR OTHER SIMILAR COSTS.
 *****************************************************************************/
// DOM-IGNORE-END

#ifndef __HTTPPRINT_H_
#define __HTTPPRINT_H_

/*****************************************************************************
 * SECTION:  Includes
 *****************************************************************************/
#include "tcpip/tcpip.h"

/*****************************************************************************
 * SECTION:  Global Variables
 *****************************************************************************/
#if defined(STACK_USE_HTTP2_SERVER)

extern HTTP_STUB httpStubs[MAX_HTTP_CONNECTIONS];
extern uint8_t curHTTPID;

/*****************************************************************************
 * SECTION:  Generated Function Prototypes
 *****************************************************************************/
void HTTPPrint(uint32_t callbackID);
void HTTPPrint_Apps(void);
void HTTPPrint_status(uint16_t);
void HTTPPrint_devid(void);
void HTTPPrint_ipaddr(void);
void HTTPPrint_brokerip(void);
void HTTPPrint_constate(void);
void HTTPPrint_mqttvers(void);
void HTTPPrint_version(void);
void HTTPPrint_chloride_weight(void);
void HTTPPrint_http(void);
void HTTPPrint_mqtt(void);
void HTTPPrint_chloride_warn(void);
void HTTPPrint_chloride_empty(void);
void HTTPPrint_status_fail(void);
void HTTPPrint_large_bank(void);
void HTTPPrint_small_bank(void);
void HTTPPrint_bank_no(void);
void HTTPPrint_status_OUT(uint16_t);
void HTTPPrint_IPAddress(void);
void HTTPPrint_Subnet(void);
void HTTPPrint_Gateway(void);
void HTTPPrint_DNS(void);
void HTTPPrint_dhcp(void);
void HTTPPrint_modbus_tcp(void);

/*****************************************************************************
 * FUNCTION: HTTPPrint
 *
 * RETURNS:  None
 *
 * PARAMS:   callbackID
 *****************************************************************************/
void HTTPPrint(uint32_t callbackID)
{
   switch(callbackID)
   {
        case 0x00000000:
			HTTPPrint_Apps();
			break;
        case 0x00000001:
			HTTPPrint_status(0);
			break;
        case 0x00000002:
			HTTPPrint_status(1);
			break;
        case 0x00000003:
			HTTPPrint_status(2);
			break;
        case 0x00000004:
			HTTPPrint_status(3);
			break;
        case 0x00000005:
			HTTPPrint_devid();
			break;
        case 0x00000006:
			HTTPPrint_ipaddr();
			break;
        case 0x00000007:
			HTTPPrint_brokerip();
			break;
        case 0x00000008:
			HTTPPrint_constate();
			break;
        case 0x00000009:
			HTTPPrint_mqttvers();
			break;
        case 0x0000000a:
			HTTPPrint_version();
			break;
        case 0x0000000b:
			HTTPPrint_chloride_weight();
			break;
        case 0x0000000d:
			HTTPPrint_http();
			break;
        case 0x0000000e:
			HTTPPrint_mqtt();
			break;
        case 0x0000000f:
			HTTPPrint_chloride_warn();
			break;
        case 0x00000010:
			HTTPPrint_chloride_empty();
			break;
        case 0x00000011:
			HTTPPrint_status_fail();
			break;
        case 0x00000012:
			HTTPPrint_large_bank();
			break;
        case 0x00000013:
			HTTPPrint_small_bank();
			break;
        case 0x00000014:
			HTTPPrint_bank_no();
			break;
        case 0x00000015:
			HTTPPrint_status(4);
			break;
        case 0x00000016:
			HTTPPrint_status(5);
			break;
        case 0x00000017:
			HTTPPrint_status(6);
			break;
        case 0x00000018:
			HTTPPrint_status(7);
			break;
        case 0x00000019:
			HTTPPrint_status_OUT(0);
			break;
        case 0x0000001a:
			HTTPPrint_status_OUT(1);
			break;
        case 0x0000001b:
			HTTPPrint_status_OUT(2);
			break;
        case 0x0000001c:
			HTTPPrint_status_OUT(3);
			break;
        case 0x0000001d:
			HTTPPrint_status_OUT(4);
			break;
        case 0x0000001e:
			HTTPPrint_status_OUT(5);
			break;
        case 0x0000001f:
			HTTPPrint_status_OUT(6);
			break;
        case 0x00000020:
			HTTPPrint_status_OUT(7);
			break;
        case 0x00000029:
			HTTPPrint_IPAddress();
			break;
        case 0x0000002a:
			HTTPPrint_Subnet();
			break;
        case 0x0000002b:
			HTTPPrint_Gateway();
			break;
        case 0x0000002c:
			HTTPPrint_DNS();
			break;
        case 0x0000002d:
			HTTPPrint_dhcp();
			break;
        case 0x0000002e:
			HTTPPrint_modbus_tcp();
			break;
       default:
           // Output notification for undefined values
           TCPPutROMArray(sktHTTP, (ROM uint8_t*)"!DEF", 4);
   }

   return;
}

void HTTPPrint_(void)
{
   TCPPut(sktHTTP, '~');
   return;
}

#endif /*STACK_USE_HTTP2_SERVER*/

#endif /*__HTTPPRINT_H_*/
